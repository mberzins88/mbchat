mainApp.controller("messageController", ['$scope','$http',
	function($scope, $http) {
		$scope.sendNotificationMessage = function() {
			if($scope.messageInput != '') {
					var chatMessageData = {						
					text : $scope.messageInput
				};
				$scope.messageInput = '';
				$http.post('/chat/sendNotificationMessage', chatMessageData)
			}
		};
		
		$scope.displayMessages = function(fcmMessages) {
			$scope.fcmMessages = fcmMessages;
			$scope.$apply();
		};
	}
]);
