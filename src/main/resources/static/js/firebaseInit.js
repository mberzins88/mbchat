var config = {
	apiKey: "AIzaSyCLfTdfMFxWqV46cA1hhCkkci2GsUHNO0w",
	authDomain: "mbchat-7ec15.firebaseapp.com",
	databaseURL: "https://mbchat-7ec15.firebaseio.com",
	projectId: "mbchat-7ec15",
	storageBucket: "mbchat-7ec15.appspot.com",
	messagingSenderId: "22479423778"
};
var fcmReceivedMessages = [];

firebase.initializeApp(config); 

const messaging = firebase.messaging();
messaging.requestPermission()
	.then(function() {	
		return messaging.getToken();
	})
	.then(function(token) {
		fetch('https://iid.googleapis.com/iid/v1/'+token+'/rel/topics/testchat', {
		  method: 'POST',
		  headers: new Headers({
		    'Authorization': 'key=AAAABTvgySI:APA91bGX_9BPetaclAy456ZfFDu1UT3cfsAn5-HKxfLaMMRIPCWIshOA4wybpSi5Vn4AvEd902VFJ6i7nZ2v_fyhSAUn2op3zAWucvY8uu_Ia2d-mJxm1wMBhnE5MOTAsitC0btO7ykZ'
		  })
		});
	})
	.catch(function(err) {			
		console.log('An error occurred while retrieving token. ', err);
	});
messaging.onMessage(function(payload) {
	fcmReceivedMessages.push(payload);
	angular.element(document.getElementById('ChatBlock')).scope().displayMessages(fcmReceivedMessages);
})