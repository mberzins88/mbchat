importScripts('https://www.gstatic.com/firebasejs/4.1.2/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/4.1.2/firebase-messaging.js')

var config = {
	apiKey: "AIzaSyCLfTdfMFxWqV46cA1hhCkkci2GsUHNO0w",
	authDomain: "mbchat-7ec15.firebaseapp.com",
	databaseURL: "https://mbchat-7ec15.firebaseio.com",
	projectId: "mbchat-7ec15",
	storageBucket: "mbchat-7ec15.appspot.com",
	messagingSenderId: "22479423778"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
	  const notificationTitle = 'Saņemta jauna ziņa';
	  const notificationOptions = {
	    body: payload.data.text,
	    icon: '/images/ico_mbchat.png'
	  };

	  return self.registration.showNotification(notificationTitle,
	      notificationOptions);
	});