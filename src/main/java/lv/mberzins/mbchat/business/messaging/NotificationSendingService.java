package lv.mberzins.mbchat.business.messaging;

import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import lv.mberzins.mbchat.dtos.ChatMessage;

@Service
public class NotificationSendingService {
	private static final String FCM_SERVER_KEY = "AAAABTvgySI:APA91bGX_9BPetaclAy456ZfFDu1UT3cfsAn5-HKxfLaMMRIPCWIshOA4wybpSi5Vn4AvEd902VFJ6i7nZ2v_fyhSAUn2op3zAWucvY8uu_Ia2d-mJxm1wMBhnE5MOTAsitC0btO7ykZ";
	private static final String FCM_NOTIFICATION_URL = "https://fcm.googleapis.com/fcm/send";
	private static final String FCM_CHAT_TOPIC = "/topics/testchat";
	
	private static final String DATE_TIME_FORMAT = "yyyy.MM.dd. HH:mm:ss";

	public void createAndSendNotification(ChatMessage chatMessage) {

		HttpsURLConnection httpsConnection = null;
		try {
			httpsConnection = (HttpsURLConnection) new URL(FCM_NOTIFICATION_URL).openConnection();
			httpsConnection.setDoOutput(true);
			httpsConnection.setDoInput(true);
			httpsConnection.setRequestMethod("POST");
			httpsConnection.setRequestProperty("Authorization", "key=" + FCM_SERVER_KEY);
			httpsConnection.setRequestProperty("Content-Type", "application/json");

			byte[] outputBytes = createSendableMessage(chatMessage).getBytes("UTF-8");
			OutputStream os = httpsConnection.getOutputStream();
			os.write(outputBytes);
			os.flush();
			os.close();
			httpsConnection.getInputStream();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (httpsConnection != null)
				httpsConnection.disconnect();
		}
	}

	private String createSendableMessage(ChatMessage chatMessage) {
		JSONObject root = new JSONObject();
		JSONObject data = new JSONObject();
		try {
			data.put("datetime", getMessageDateTime());
			data.put("text", chatMessage.getText());
			data.put("sender_id", "22479423778");
			root.put("data", data);
			root.put("to", FCM_CHAT_TOPIC);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return root.toString();
	}
	
	private String getMessageDateTime() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT));
	}
}
