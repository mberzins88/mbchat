package lv.mberzins.mbchat.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import lv.mberzins.mbchat.business.messaging.NotificationSendingService;
import lv.mberzins.mbchat.dtos.ChatMessage;

@Controller
@RequestMapping("/chat")

public class MessagingController {
	@Resource
	private NotificationSendingService notificationSendingService;

	@RequestMapping(value = "/sendNotificationMessage", method = RequestMethod.POST)
	@ResponseBody
	public void createAndSendNotifiaction(@RequestBody ChatMessage chatMessage) throws Exception {
		notificationSendingService.createAndSendNotification(chatMessage);
	}
}
