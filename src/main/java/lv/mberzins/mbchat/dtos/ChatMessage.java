package lv.mberzins.mbchat.dtos;

public class ChatMessage {
	private String text;

	public void setText(String text) {
		this.text = text;
	}

	public String getText() {
		return this.text;
	}
}
