package lv.mberzins.mbchat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan("lv.mberzins.mbchat")
@SpringBootApplication
public class MbchatApplication {

	public static void main(String[] args) {
		SpringApplication.run(MbchatApplication.class, args);
	}
}
